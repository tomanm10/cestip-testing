import py_eureka_client.eureka_client as eureka_client
import time

cache = {
    "config": {
        "time_to_ask_again": 60 * 1000000000 #60 seconds
    },

    "travel_agencies": {
        "data": None,
        "last_fetched_time": None
    },

    "terms": {
        "data": None,
        "last_fetched_time": None,
        "args": None
    }
}

def get_travel_agencies():
    global cache

    if ((cache["travel_agencies"]["data"] is None)
        or ((time.time_ns() - cache["travel_agencies"]["last_fetched_time"]) > cache["config"]["time_to_ask_again"])):

        cache["travel_agencies"]["data"] = eureka_client.do_service("dataService", "/travelagencies", return_type="json")
        cache["travel_agencies"]["last_fetched_time"] = time.time_ns()

        #print("CACHE MISS")

    else:
        #print("CACHE HIT")
        pass

    return cache["travel_agencies"]["data"]

def get_terms(args):
    global cache

    if ((cache["terms"]["data"] is None)
        or ((time.time_ns() - cache["terms"]["last_fetched_time"]) > cache["config"]["time_to_ask_again"])
        or (cache["terms"]["args"] != args)):

        requestURL = "/terms?"

        for arg_name in args:
            if args[arg_name] is not None:
                requestURL += (arg_name + "=" + str(args[arg_name]) + "&")

        requestURL = requestURL[:-1] #remove the last &

        cache["terms"]["data"] = eureka_client.do_service("dataService",
            requestURL,
            return_type="json")

        cache["terms"]["args"] = args
        cache["terms"]["last_fetched_time"] = time.time_ns()

        #print("CACHE MISS")

    else:
        #print("CACHE HIT")
        pass

    #print("TERMS")
    #print(cache["terms"]["data"][0]["from"])
    return cache["terms"]["data"]

def remap_terms_evo(travel_agencies, terms):
    mapped_data = {}

    for travel_agency in travel_agencies:
        mapped_data[travel_agency["name"]] = {}

    for term in terms:
        if term["downloadDate"] not in mapped_data[term["trip"]["travelAgency"]["name"]]:
            mapped_data[term["trip"]["travelAgency"]["name"]][term["downloadDate"]] = []

        mapped_data[term["trip"]["travelAgency"]["name"]][term["downloadDate"]].append(term["price"])

    return mapped_data

def average_prices_evo(args):
    travel_agencies = get_travel_agencies()

    terms = get_terms(args)

    mapped_data = remap_terms_evo(travel_agencies, terms)

    avg_prices = []

    for travel_agency in travel_agencies:
        data_points = []

        for date in mapped_data[travel_agency["name"]]:
            prices = mapped_data[travel_agency["name"]][date]

            prices_sum = 0
            for price in prices:
                prices_sum += price

            prices_avg = prices_sum / len(prices)

            data_point = {
                "price": prices_avg,
                "date": date
            }

            data_points.append(data_point)

        ta_data = {
            "travelAgency": travel_agency["name"],
            "prices": data_points
        }

        avg_prices.append(ta_data)

    return avg_prices

def min_prices_evo(args):
    travel_agencies = get_travel_agencies()
    terms = get_terms(args)

    mapped_data = remap_terms_evo(travel_agencies, terms)

    min_prices = []

    for travel_agency in travel_agencies:
        data_points = []

        for date in mapped_data[travel_agency["name"]]:
            prices = mapped_data[travel_agency["name"]][date]

            min_price = float('inf')
            for price in prices:
                if price < min_price:
                    min_price = price

            data_point = {
                "price": min_price,
                "date": date
            }

            data_points.append(data_point)

        ta_data = {
            "travelAgency": travel_agency["name"],
            "prices": data_points
        }

        min_prices.append(ta_data)

    return min_prices

def max_prices_evo(args):
    travel_agencies = get_travel_agencies()
    terms = get_terms(args)

    mapped_data = remap_terms_evo(travel_agencies, terms)

    max_prices = []

    for travel_agency in travel_agencies:
        data_points = []

        for date in mapped_data[travel_agency["name"]]:
            prices = mapped_data[travel_agency["name"]][date]

            max_price = float('-inf')
            for price in prices:
                if price > max_price:
                    max_price = price

            data_point = {
                "price": max_price,
                "date": date
            }

            data_points.append(data_point)

        ta_data = {
            "travelAgency": travel_agency["name"],
            "prices": data_points
        }

        max_prices.append(ta_data)

    return max_prices

def remap_terms(travel_agencies, terms):
    mapped_data = {}

    for travel_agency in travel_agencies:
        mapped_data[travel_agency["name"]] = []

    for term in terms:
        mapped_data[term["trip"]["travelAgency"]["name"]].append(term["price"])

    return mapped_data

def average_prices(args):
    travel_agencies = get_travel_agencies()
    terms = get_terms(args)

    mapped_data = remap_terms(travel_agencies, terms)

    avg_prices = []

    for ta_name in mapped_data:

        #if len(mapped_data[ta_name]) == 0: #should return data unavailable
        #    continue

        prices_sum = 0
        for price in mapped_data[ta_name]:
            prices_sum += price

        avg_price = prices_sum / len(mapped_data[ta_name])

        ta_data = {
            "travelAgency": ta_name,
            "price": avg_price
        }

        avg_prices.append(ta_data)

    return avg_prices

def min_prices(args):
    travel_agencies = get_travel_agencies()
    terms = get_terms(args)

    mapped_data = remap_terms(travel_agencies, terms)

    min_prices = []

    for ta_name in mapped_data:

        min_price = float('inf')
        for price in mapped_data[ta_name]:
            if price < min_price:
                min_price = price

        ta_data = {
            "travelAgency": ta_name,
            "price": min_price
        }

        min_prices.append(ta_data)

    return min_prices

def max_prices(args):
    travel_agencies = get_travel_agencies()
    terms = get_terms(args)

    mapped_data = remap_terms(travel_agencies, terms)

    max_prices = []

    for ta_name in mapped_data:

        max_price = float('-inf')
        for price in mapped_data[ta_name]:
            if price > max_price:
                max_price = price

        ta_data = {
            "travelAgency": ta_name,
            "price": max_price
        }

        max_prices.append(ta_data)

    return max_prices

def count_terms(args):
    travel_agencies = get_travel_agencies()
    terms = get_terms(args)

    mapped_data = remap_terms(travel_agencies, terms)

    term_count = []

    for ta_name in mapped_data:
        count = len(mapped_data[ta_name])

        ta_data = {
            "travelAgency": ta_name,
            "price": count
        }

        term_count.append(ta_data)

    return term_count
