import py_eureka_client.eureka_client as eureka_client
from datetime import datetime, timedelta
import urllib
import sys
import json
import math

from service import statistics

rest_server_port = 5000
# The folowing code will register your server to eureka server and also start to send heartbeat every 30 seconds
eureka_client.init(eureka_server = "http://cestip.tk:8761/eureka/",
                   app_name = "statisticsService",
                   instance_port = rest_server_port)

def compare_jsons(expected, actual):
    expected_dump = json.dumps(expected, sort_keys=True)
    actual_dump = json.dumps(actual, sort_keys=True)

    return expected_dump == actual_dump

def assert_equal_evo(expected, actual, test):
    equal = compare_jsons(expected, actual)
    if not equal:
        if len(expected) > 0 and len(expected[0]["prices"]) > 0:
            expected_datapoint = expected[0]["prices"][0]

            for actual_datapoint in actual[0]["prices"]:
                if actual_datapoint["date"].split("T")[0] == expected_datapoint["date"].split("T")[0]: #only compare yyyy-mm-dd
                    if math.floor(actual_datapoint["price"]) == math.floor(expected_datapoint["price"]):
                        print(test, "PASSED")
                        return
                    else:
                        print(test, "FAILED")
                        print("Expected datapoint:")
                        print(expected_datapoint)
                        print("Actual datapoint:")
                        print(actual_datapoint)
                        return

                """
                if compare_jsons(expected, actual):
                    print(test, "PASSED")
                    return
                """
            print(test, "FAILED")
            print("Expected datapoints:")
            print(expected[0]["prices"])
            print("Actual datapoints:")
            print(actual[0]["prices"])

        else:
            print(test, "FAILED")

    else:
        print(test, "PASSED")

def avg_evolution_test():
    args = {}

    #args["from"] = (datetime.now() - timedelta(days=40)).strftime('%Y-%m-%d')
    #args["to"] = (datetime.today() + timedelta(days=40)).strftime('%Y-%m-%d')

    args["downloadDateFrom"] = (datetime.now() - timedelta(days=7)).strftime('%Y-%m-%d')
    args["downloadDateTo"] = datetime.today().strftime('%Y-%m-%d')

    try:
        expected = statistics.average_prices_evo(args)

        requestURL = "/statistics/prices/avg/evolution?"
        for arg_name in args:
            if args[arg_name] is not None:
                requestURL += (arg_name + "=" + str(args[arg_name]) + "&")
        requestURL = requestURL[:-1] #remove the last &

        actual = eureka_client.do_service("dataService", requestURL, return_type="json")

    except urllib.request.HTTPError as e:
        # If all nodes are down, an `HTTPError` will be raised
        print("Service unavailable")
        return 503

    assert_equal_evo(expected, actual, "AVG EVO TEST")

def min_evolution_test():
    args = {}
    args["downloadDateFrom"] = (datetime.now() - timedelta(days=7)).strftime('%Y-%m-%d')
    args["downloadDateTo"] = datetime.today().strftime('%Y-%m-%d')

    try:
        expected = statistics.min_prices_evo(args)

        requestURL = "/statistics/prices/min/evolution?"
        for arg_name in args:
            if args[arg_name] is not None:
                requestURL += (arg_name + "=" + str(args[arg_name]) + "&")
        requestURL = requestURL[:-1] #remove the last &

        actual = eureka_client.do_service("dataService", requestURL, return_type="json")

    except urllib.request.HTTPError as e:
        # If all nodes are down, an `HTTPError` will be raised
        print("Service unavailable")
        return 503

    assert_equal_evo(expected, actual, "MIN EVO TEST")

def max_evolution_test():
    args = {}
    args["downloadDateFrom"] = (datetime.now() - timedelta(days=7)).strftime('%Y-%m-%d')
    args["downloadDateTo"] = datetime.today().strftime('%Y-%m-%d')

    try:
        expected = statistics.max_prices_evo(args)

        requestURL = "/statistics/prices/max/evolution?"
        for arg_name in args:
            if args[arg_name] is not None:
                requestURL += (arg_name + "=" + str(args[arg_name]) + "&")
        requestURL = requestURL[:-1] #remove the last &

        actual = eureka_client.do_service("dataService", requestURL, return_type="json")

    except urllib.request.HTTPError as e:
        # If all nodes are down, an `HTTPError` will be raised
        print("Service unavailable")
        return 503

    assert_equal_evo(expected, actual, "MAX EVO TEST")

def assert_equal(expected, actual, test):
    equal = compare_jsons(expected, actual)
    if not equal:
        if len(expected) > 10:
            expected_datapoint = expected[10]

            for actual_datapoint in actual:
                if actual_datapoint["travelAgency"] == expected_datapoint["travelAgency"]:
                    if math.floor(actual_datapoint["price"]) == math.floor(expected_datapoint["price"]):
                        print(test, "PASSED")
                        return

            print(test, "FAILED")
            print("Expected datapoints:")
            print(expected)
            print("Actual datapoints:")
            print(actual)

        else:
            print(test, "FAILED")

    else:
        print(test, "PASSED")

def avg_test():
    args = {}
    args["downloadDateFrom"] = (datetime.now() - timedelta(days=7)).strftime('%Y-%m-%d')
    args["downloadDateTo"] = datetime.today().strftime('%Y-%m-%d')

    try:
        expected = statistics.average_prices(args)

        requestURL = "/statistics/prices/avg?"
        for arg_name in args:
            if args[arg_name] is not None:
                requestURL += (arg_name + "=" + str(args[arg_name]) + "&")
        requestURL = requestURL[:-1] #remove the last &

        actual = eureka_client.do_service("dataService", requestURL, return_type="json")

    except urllib.request.HTTPError as e:
        # If all nodes are down, an `HTTPError` will be raised
        print("Service unavailable")
        return 503

    assert_equal(expected, actual, "AVG TEST")

def min_test():
    args = {}
    args["downloadDateFrom"] = (datetime.now() - timedelta(days=7)).strftime('%Y-%m-%d')
    args["downloadDateTo"] = datetime.today().strftime('%Y-%m-%d')

    try:
        expected = statistics.min_prices(args)

        requestURL = "/statistics/prices/min?"
        for arg_name in args:
            if args[arg_name] is not None:
                requestURL += (arg_name + "=" + str(args[arg_name]) + "&")
        requestURL = requestURL[:-1] #remove the last &

        actual = eureka_client.do_service("dataService", requestURL, return_type="json")

    except urllib.request.HTTPError as e:
        # If all nodes are down, an `HTTPError` will be raised
        print("Service unavailable")
        return 503

    assert_equal(expected, actual, "MIN TEST")

def max_test():
    args = {}
    args["downloadDateFrom"] = (datetime.now() - timedelta(days=7)).strftime('%Y-%m-%d')
    args["downloadDateTo"] = datetime.today().strftime('%Y-%m-%d')

    try:
        expected = statistics.max_prices(args)

        requestURL = "/statistics/prices/max?"
        for arg_name in args:
            if args[arg_name] is not None:
                requestURL += (arg_name + "=" + str(args[arg_name]) + "&")
        requestURL = requestURL[:-1] #remove the last &

        actual = eureka_client.do_service("dataService", requestURL, return_type="json")

    except urllib.request.HTTPError as e:
        # If all nodes are down, an `HTTPError` will be raised
        print("Service unavailable")
        return 503

    assert_equal(expected, actual, "MAX TEST")

# this must be at the end of the file
if __name__ == "__main__":
    avg_evolution_test()
    min_evolution_test()
    max_evolution_test()

    avg_test()
    min_test()
    max_test()
