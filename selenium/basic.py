# basic tests
def title_tests(driver):
    includes = ["CesTip", "dovolená"]

    for word in includes:
        assert word in driver.title,\
            "Title does not include '{}'".format(word)

    not_includes = ["Čedok", "Invia"]
    for word in not_includes:
        assert word not in driver.title,\
            "Title includes '{}'".format(word)

tests = { 
    "Title string tests": title_tests
}