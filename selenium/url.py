from selenium.webdriver import ActionChains

# url changes
def select_box_tests(driver):
    keys = [ "destination", "food", "travelType", "travelers" ]

    for key in keys: select_box_test(key, driver)

def select_box_test(input_id, driver):
    strava = driver.find_element_by_css_selector(
        " label[for='{}']~div .css-1hwfws3"\
            .format(input_id)
    )

    last_url = driver.current_url
    last_option_text = ""

    strava.click()
    options = driver.find_elements_by_class_name("react-select__option")

    for i in range(len(options)):
        if i > 10: break # unnecessary tests
        
        # have to reload options object, Selenium needs it
        options = driver.find_elements_by_class_name("react-select__option") 

        option = options[i]
        current_option_text = option.text
        option.click()

        assert driver.current_url != last_url,\
        "Select box {}, switching from '{}' to '{}', URL's don't differ"\
        .format(input_id, last_option_text, current_option_text)

        last_url = driver.current_url
        last_option_text = current_option_text

        strava.click()

def price_slider_test(driver):
    PRICE_TOOLTIP_CLASS = "rc-slider-tooltip-inner"
    LEFT_HANDLE_CLASS = "rc-slider-handle-1"
    RIGHT_HANDLE_CLASS = "rc-slider-handle-2"

    left_handle = driver.find_elements_by_class_name(LEFT_HANDLE_CLASS)[0]
    right_handle = driver.find_elements_by_class_name(RIGHT_HANDLE_CLASS)[0]

    hover_action = ActionChains(driver)
    hover_action.move_to_element(left_handle).perform()

    price_tooltip = driver.find_elements_by_class_name(PRICE_TOOLTIP_CLASS)[0]

    move = ActionChains(driver)

    price = int(price_tooltip.text[0:-4])
    last_price = price
    last_url = ""

    while price  < 50000:
        current_url = driver.current_url

        move.click_and_hold(left_handle)\
            .move_by_offset(10, 0).release()\
                .perform()

        price = int(price_tooltip.text[0:-4])

        assert last_url != current_url,\
            "Changing min-price from {} to {} and URL did not change"\
                .format(last_price, price)

        last_price = price

    driver.find_element_by_css_selector("label").click() # ground


    hover_action = ActionChains(driver)
    hover_action.move_to_element(right_handle).perform()

    price_tooltip = driver.find_elements_by_class_name(PRICE_TOOLTIP_CLASS)[1]
    price = int(price_tooltip.text[0:-4])
    print(price)
    last_price = price
    last_url = ""
    
    move = ActionChains(driver)

    while price  > 100000:
        current_url = driver.current_url

        move.click_and_hold(right_handle)\
            .move_by_offset(-10, 0).release()\
                .perform()

        price = int(price_tooltip.text[0:-4])

        assert last_url != current_url,\
            "Changing min-price from {} to {} and URL did not change"\
                .format(last_price, price)

        last_price = price

    

tests = { 
    "URL changes on select boxes" : select_box_tests,
    "Price slider works and changes URL": price_slider_test
}