import importlib
from selenium import webdriver

GREEN = "\033[92m"
RED = "\033[91m"
END_COLOR = "\033[0m"

tests = {}

def import_tests(module_name):
    module = importlib.import_module(module_name)
    tests.update(module.tests)

def run_all_tests(origin, close_after = False):
    driver = webdriver.Chrome()
    driver.get(origin)

    total_tests = len(tests)
    tests_failed = 0
    tests_ran = 0

    for test_name in tests:
        tests_ran += 1

        try: 
            tests[test_name](driver)
        except AssertionError as e:
            print("{} Test '{}' failed\n{}Reason: {}"\
                .format(RED, test_name, END_COLOR, str(e)))
            tests_failed += 1

    tests_passed = tests_ran - tests_failed
    percentage = 100 * tests_passed/tests_ran
    color = GREEN if (percentage == 100) else RED

    print("\n{} tests ran, {} passed, {} failed – {} {} % {} passed"\
        .format(tests_ran, tests_passed, tests_failed,\
        color, percentage, END_COLOR))

    if(close_after): driver.close()